function Param = ADMM(Param)
%
%ADMM
%
%This function performs the ADMM optimization using the parameters included
%in the Param struct
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Param = ADMM(Param)
%   INPUTS
%       Param: Parameter struct.
%   OUTPUTS
%       Param: Parameter struct.

Param.OptimVar.x = Param.ADMM.x0;
Param.OptimVar.z = Param.ADMM.z0;
Param.OptimVar.y = Param.ADMM.y0;
Param.OptimVar.Rho = Param.ADMM.Rho0;
Param.OptimVar.iter = 1;

Param.OptimVar.History.x{1} = Param.ADMM.x0;
Param.OptimVar.History.z{1} = Param.ADMM.z0;
Param.OptimVar.History.y{1} = Param.ADMM.y0;
Param.OptimVar.History.Rho{1} = Param.ADMM.Rho0;
Param.OptimVar.History.fx{1} = 0;
Param.OptimVar.History.gz{1} = 0;
disp('Starting ADMM Optimization...');
while not(StopConditions(Param))
    [Param.OptimVar.x, Param.OptimVar.fx] = UpdateX(Param);
    Param.OptimVar.y = Param.OptimVar.y + Param.OptimVar.Rho*(Param.OptimVar.x-Param.OptimVar.z);
    [Param.OptimVar.z, Param.OptimVar.gz] = UpdateY(Param);
    Param.OptimVar.y = Param.OptimVar.y + Param.OptimVar.Rho*(Param.OptimVar.x-Param.OptimVar.z);
    Param = PrintAndSaveState(Param);
    [Param.OptimVar.Rho,Param.OptimVar.iter] = RhoUpdate(Param);
    Param = PauseMessage(Param);
end
end

function [xout,fval] = UpdateX(Param)

Lf = @(x)Param.ADMM.Lf(x,Param.OptimVar.z,Param.OptimVar.y,Param.OptimVar.Rho);
options = optimoptions(@fminunc,'Algorithm','trust-region',...
'GradObj','on','Hessian','on','Display','none');
%'TolFun',1,'TolX',1,'GradObj','on','Hessian','on','Display','none');

options.MaxIter = Param.ADMM.XupdMaxiter;
[xout,Lfval] = fminunc(Lf,Param.OptimVar.x,options);
fval = Lfval - ( Param.OptimVar.y'*xout + (Param.OptimVar.Rho/2)*(xout-Param.OptimVar.z)'*(xout-Param.OptimVar.z) );

end

function [zout,gval] = UpdateY(Param)
Lg = @(z)Param.ADMM.Lg(z,Param.OptimVar.x,Param.OptimVar.y,Param.OptimVar.Rho);
options = optimoptions(@fminunc,'Algorithm','trust-region',...
'GradObj','on','Hessian','on','Display','none');
%'TolFun',1,'TolX',1,'GradObj','on','Hessian','on','Display','none');
options.MaxIter = Param.ADMM.ZupdMaxiter;
[zout,Lgval] = fminunc(Lg,Param.OptimVar.z,options);
gval = Lgval - ( - Param.OptimVar.y'*zout + (Param.OptimVar.Rho/2)*(Param.OptimVar.x-zout)'*(Param.OptimVar.x-zout) ); 
end

function Stop = StopConditions(Param)
r = (Param.OptimVar.x-Param.OptimVar.z);
Stop = ((r'*r<Param.ADMM.Epsilon) || (Param.OptimVar.iter > Param.ADMM.Maxiter)) && not(Param.OptimVar.iter == 1);
end

function [Rho,iter] = RhoUpdate(Param)
r = (Param.OptimVar.x-Param.OptimVar.z);
s =  Param.OptimVar.Rho*(Param.OptimVar.z-Param.OptimVar.History.z{Param.OptimVar.iter});
if Param.OptimVar.iter < Param.ADMM.ConvTrigger
   
    if r'*r > Param.ADMM.MUinc^2*(s'*s)
        Rho = Param.OptimVar.Rho*Param.ADMM.TAUinc;
    elseif s'*s > Param.ADMM.MUdec^2*(r'*r)
        Rho = Param.OptimVar.Rho/Param.ADMM.TAUdec;
    else
        Rho = Param.OptimVar.Rho;
    end
  
elseif Param.OptimVar.iter == Param.ADMM.ConvTrigger
    disp('Forcing Convergence...')
    Rho = Param.OptimVar.Rho*Param.ADMM.TAUinc;
else 
    Rho = Param.OptimVar.Rho*Param.ADMM.TAUinc;
end
iter = Param.OptimVar.iter + 1;
end

function Param = PrintAndSaveState(Param)

resultstring(1) = Param.OptimVar.fx;
resultstring(2) = Param.OptimVar.gz;
resultstring(3) = norm(Param.OptimVar.x-Param.OptimVar.z,2);
resultstring(4) = Param.OptimVar.fx + Param.OptimVar.gz + Param.OptimVar.y'*(Param.OptimVar.x-Param.OptimVar.z) + ...
    Param.OptimVar.Rho/2*(Param.OptimVar.x-Param.OptimVar.z)'*(Param.OptimVar.x-Param.OptimVar.z);
resultstring(5) = Param.OptimVar.Rho;
resultstring(6) = Param.OptimVar.iter;

if Param.OptimVar.iter == 1
    disptable(resultstring, 'f(x)|g(z)|norm(x-z)|Lagrangian|Rho|iter',[],5,10)
else
   disptable(resultstring,[],[],5,10)
end

Param.OptimVar.History.x{Param.OptimVar.iter+1} = Param.OptimVar.x;
Param.OptimVar.History.z{Param.OptimVar.iter+1} = Param.OptimVar.z;
Param.OptimVar.History.y{Param.OptimVar.iter+1} = Param.OptimVar.y;
Param.OptimVar.History.Rho{Param.OptimVar.iter+1} = Param.OptimVar.Rho;
Param.OptimVar.History.fx{Param.OptimVar.iter+1} = Param.OptimVar.fx;
Param.OptimVar.History.gz{Param.OptimVar.iter+1} = Param.OptimVar.gz;

end

function Param = PauseMessage(Param)
    if mod(Param.OptimVar.iter,Param.ADMM.iterpause) == 0
        
        str = input('Press C to continue, Press S to save results and Stop ADMM, Press F to force Convergence \n','s');
        while not(strcmpi(str,'C') || strcmpi(str,'S') || strcmpi(str,'F'))
            str = input('Input not recognized. Press C to continue, Press S to save results and Stop ADMM \n','s');
        end
        if strcmpi(str,'S')
            Param.OptimVar.iter = Param.ADMM.Maxiter+1;
        elseif strcmpi(str,'F')
            Param.ADMM.ConvTrigger= Param.OptimVar.iter;
        else
            %disp('Optimizing Water Parameters');
            %Param = OptimizeWaterParameters(Param);
            %Param.ADMM.Lf = @(x,z,y,Rho)FittingStateEval(x,z,y,Rho,Param);
            %Param.ADMM.Lg = @(z,x,y,Rho)SpringStateEval(z,x,y,Rho,Param);
            
        end
    end
end
function Param = SetWeights(alpha,Param)
%
%SetWeights
%
%This function sets the relative wieghting between the different terms of
%the Lagrangian according to each term relatitve order of magnitude and
%the weighting parameter alpha.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Param = SetWeights(alpha,Param)
%   INPUTS
%       alpha: Relative weight between SAXS and ESM functions.
%       Param: Parameter struct.
%   OUTPUTS
%       Param: Parameter struct.

disp('Setting Optimization Weights...');
LagrangianWeight = 2*Param.ADMM.Rho0*norm(Param.Anchoring.xl)^2;
SpringWeight = 2*Param.Spring.h'.*Param.Spring.K*Param.Spring.h;
[~,Iest,~,~] = SAXSFeval(Param.Start.Structure,Param.Fitting.F,Param.Target.Iexp,Param.Start.Nres,Param.Target.q);
FittingWeight = 2*norm(Param.Target.Iexp)*norm(Iest);


Param.Fitting.Fc = alpha*LagrangianWeight/FittingWeight;
Param.Spring.Kc = (1-alpha)*LagrangianWeight/SpringWeight;
disp('Optimization Weights Set...');
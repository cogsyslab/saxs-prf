%% Show Results
function ShowResults(Param)
%Plot initial and final structure
N = Param.Start.Nres;
ZPMat = Param.Anchoring.ZPMat;
ZFVec = Param.Anchoring.ZFVec;
Indx = Param.Spring.Indx;
figure
subplot(1,2,1)
C1 = reshape(ZPMat*Param.ADMM.x0+ZFVec,3,N)';
scatter3(C1(:,1),C1(:,2),C1(:,3),[],1:length(C1),'filled')
for i = 1:length(Indx)
line(C1([Indx(i,1),Indx(i,2)],1),C1([Indx(i,1),Indx(i,2)],2),C1([Indx(i,1),Indx(i,2)],3),'Color','r')
end
axis equal tight
subplot(1,2,2)
C2 = reshape(ZPMat*Param.OptimVar.x+ZFVec,3,N)';
scatter3(C2(:,1),C2(:,2),C2(:,3),[],1:length(C2),'filled')
for i = 1:length(Indx)
line(C2([Indx(i,1),Indx(i,2)],1),C2([Indx(i,1),Indx(i,2)],2),C2([Indx(i,1),Indx(i,2)],3),'Color','r')
end
axis equal tight
%% Plot initial and final Structure with water layer
[S1,Fs1] = HydrationShellv2(C1,Param.Water.NSubd,Param.Water.NSam,Param.Water.WSThk,Param.Water.SCThk,Param.Fitting.F,Param.Target.q,Param);
[S2,Fs2] = HydrationShellv2(C2,Param.Water.NSubd,Param.Water.NSam,Param.Water.WSThk,Param.Water.SCThk,Param.Fitting.F,Param.Target.q,Param);
figure
subplot(1,2,1)
C1 = Param.Start.Structure;
scatter3(C1(:,1),C1(:,2),C1(:,3),[],1:length(C1),'filled')
for i = 1:length(Indx)
line(C1([Indx(i,1),Indx(i,2)],1),C1([Indx(i,1),Indx(i,2)],2),C1([Indx(i,1),Indx(i,2)],3),'Color','r')
end
hold on
scatter3(S1(N+1:end,1),S1(N+1:end,2),S1(N+1:end,3),[],'b','filled');
axis equal tight

subplot(1,2,2)
C2 = reshape(ZPMat*Param.OptimVar.x+ZFVec,3,N)';
scatter3(C2(:,1),C2(:,2),C2(:,3),[],1:length(C2),'filled')
for i = 1:length(Indx)
line(C2([Indx(i,1),Indx(i,2)],1),C2([Indx(i,1),Indx(i,2)],2),C2([Indx(i,1),Indx(i,2)],3),'Color','r')
end
hold on
scatter3(S2(N+1:end,1),S2(N+1:end,2),S2(N+1:end,3),[],'b','filled');
axis equal tight
%% Plot Scattering Fit
figure
Iexp = Param.Target.Iexp;
q = Param.Target.q;
[sqne,Iest,e,D] = FittingObjWat(S2,Fs2,Iexp,N,q);
[sqne,Istart,e,D] = FittingObjWat(S1,Fs1,Iexp,N,q);
Iexp = Iexp/Iexp(1)*Iest(1);
plot(q,log10(Iexp),'r')
hold on
plot(q,log10(Iest),'--b')
plot(q,log10(Istart),'--g')
%%
[U, r, lrms] = Kabsch(C1', C2');
disp(['RMSD: ', num2str(lrms)]);

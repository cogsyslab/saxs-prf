function ADMM = ADMMOptions(varargin)
%
%ADMMptions
%
%This function loads the ADMM section of the Param struct with the
%options relative to the ADMM module.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%ADMM = ADMMOptions(varargin)
%   INPUTS
%       Param: Parameter struct.
%       name: String with the field to be changed.
%       value: Value to be supplied to that field.
%   OUTPUTS
%       ADMM: Struct with the ADMM options

Param = varargin{1}; %Param Struct is the first variable
ADMM.Lf = @(x,z,y,Rho)SAXSeval(x,z,y,Rho,Param);
ADMM.Lg = @(z,x,y,Rho)ESMeval(z,x,y,Rho,Param);

ADMMFields = {'MUinc';'MUdec';'TAUinc';'TAUdec'; ...
    'Rho0';'Maxiter';'Epsilon';'ConvTrigger'; ...
    'XupdMaxiter';'ZupdMaxiter';'x0';'z0';'y0';'iterpause'}; % Define the 
                % name of the editable fields to be written in the struct

ADMM.MUinc = 10; %Define the default parameters
ADMM.MUdec = 10; %Define the default parameters
ADMM.TAUinc = 2; %Define the default parameters
ADMM.TAUdec = 2; %Define the default parameters
ADMM.Rho0 = 1; %Define the default parameters
ADMM.Maxiter = 100; %Define the default parameters
ADMM.Epsilon = 0.01; %Define the default parameters
ADMM.ConvTrigger = 15; %Define the default parameters
ADMM.XupdMaxiter = 100; %Define the default parameters
ADMM.ZupdMaxiter = 100; %Define the default parameters
ADMM.x0 = Param.Anchoring.x; %Define the default parameters
ADMM.z0 = ADMM.x0; %Define the default parameters
ADMM.y0 = zeros(length(ADMM.x0),1); %Define the default parameters
ADMM.iterpause = 100; 

%Rewrite those that the user especified
for field = 1:length(ADMMFields)
    if sum(strcmpi(ADMMFields{field},varargin)) > 0 %If there is a coincidence with the input and a field name
        ValueIndx = find(strcmpi(ADMMFields{field},varargin) == 1,1,'first'); %Search which is the coincidence
        ADMM.(ADMMFields{field}) = varargin{ValueIndx + 1}; %Edit that field with the value supplied by the user
    end
end


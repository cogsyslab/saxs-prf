function F = AminoFormFactor(labels,q)
%
%AminoFormFactor
%
%This function returns the form factors of each amino acid as a function of
%the amino acid three letter label.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%F = AminoFormFactor(labels,q)
%   INPUTS
%       labels: Cell of three letter strings with the amino acid label.
%       q: Vector of SAXS q scattering angle values.
%   OUTPUTS
%       F: Matrix with the form factor of each amino acid.



N = size(labels,1);
Q = length(q);
F = zeros(N,Q);

for sph = 1:N
    a = 0;
    b = 0;
    c = 0;
    d = 0;
    f = 0;
    switch labels{sph}
        case 'DS'
            a = 3.5301846863022959E+01;
            b = 8.5378345850638038E-01;
            c = -1.7507051229758559E+01;
            d = 1.7010560191799904E+00;
        case 'RS'
            a = 4.0230401562412005E+01;
            b = 1.6287412345143366E+00;
            c = -4.3118717065025962E+01;
            d = 2.4712585246295806E+01;
        case 'ADE'
            a = 3.0473077532191034E+01;
            b = 2.7267438703396873E-02;
            c = -9.4174795997194121E+00;
            d = -1.8632787145940051E+00;
        case 'CYT'
            a = 2.2594403303998384E+01;
            b = -1.3979255030930837E-01;
            c = 2.0602701684525548E-01;
            d = -4.8964867374677956E+00;
        case 'GUA'
            a = 3.5413612820294524E+01;
            b = 3.1135141139744515E-01;
            c = -1.8995234746163160E+01;
            d = 2.7546664066782145E+00;
        case 'THY'
            a = 2.1171045387193391E+01;
            b = -2.5406818426818600E-01;
            c = 8.5747955915722898E+00;
            d = -1.2569339716672346E+01;
        case 'URA'
            a = 2.2101478120069594E+01;
            b = -1.3059135164353608E-01;
            c = 1.4058971332981493E-01;
            d = -4.4641771309272844E+00;
        case 'SOL'
            a = 9.9989107859081461E+00;
            b = 7.1847528938221994E-03;
            c = -1.1436678741857631E+00;
            d = 1.3146929900084206E-01;

        case 'GLY'
            a = 9.9691190155764158E+00;
            b = -1.7528320201296514E-02;
            c = 1.6450018157708988E+00;
            d = -7.0378838370050911E-01;
            f = -6.8495906226686154E-01;
        case 'ALA'
            a = 9.0385953871608269E+00;
            b = -9.3522348098657587E-02;
            c = 7.1811578286517612E+00;
            d = -4.1100094434439107E+00;
            f = -1.5204739994705183E+00;
        case 'VAL'
            a = 7.1816423864658629E+00;
            b = -4.7643636342215978E-01;
            c = 2.6878626217077645E+01;
            d = -3.0036144646732087E+01;
            f = 7.6041571666551446E+00;
        case 'LEU'
            a = 6.2519392408994952E+00;
            b = -7.0384372758014790E-01;
            c = 4.4699858222716180E+01;
            d = -6.7435461752575918E+01;
            f = 2.8904017698517240E+01;
        case 'ILE'
            a = 6.2512251193666968E+00;
            b = -6.5569054939799687E-01;
            c = 4.2523603838224325E+01;
            d = -6.1110285327663291E+01;
            f = 2.5111019303628737E+01;
        case 'MET'
            a = 1.6545461441433780E+01;
            b = -3.0728785802722175E-01;
            c = 3.3057451408162395E+00;
            d = -1.3719988781068070E+01;
            f = 8.9621624126553510E+00;
        case 'PHE'
            a = 9.2253730534361544E+00;
            b = -1.0235816923538334E+00;
            c = 4.0056964609302334E+01;
            d = -6.6733866971096148E+01;
            f = 3.1299821660208639E+01;
        case 'TRP'
            a = 1.5684405852404351E+01;
            b = -9.4431662966303997E-01;
            c = 2.3675802486236719E+01;
            d = -4.8238875823804648E+01;
            f = 2.4504594828865912E+01;
        case 'PRO'
            a = 8.6193900746569110E+00;
            b = -3.2220720095487299E-01;
            c = 1.8997202349961341E+01;
            d = -1.6823509153368370E+01;
            f = 1.4683423688962776E+00;
        case 'SER'
            a = 1.3987709443384963E+01;
            b = -5.3597112650067072E-02;
            c = 1.6532170902823629E+00;
            d = -2.1698155818409752E+00;
            f = -1.0079162195407658E+00;
        case 'THR'
            a = 1.3058035944130433E+01;
            b = -1.6805767593043786E-01;
            c = 8.4026246743192505E+00;
            d = -7.3396417884658378E+00;
            f = -1.4446678521071559E+00;
        case 'CYS'
            a = 1.9124654812324472E+01;
            b = -5.9927863522006228E-02;
            c = -3.3836190674229298E+00;
            d = -2.4665166405439520E+00;
            f = 1.9122022270707959E+00;
        case 'TYR'
            a = 1.4149376249271270E+01;
            b = 2.3351533285852905E-01;
            c = -6.6303117968389449E+00;
            d = 7.7120460565621229E+00;
            f = 1.2754732796902342E+00;
        case 'ASN'
            a = 1.9938673457674692E+01;
            b = -5.4516265978723853E-02;
            c = -6.1462879847152454E+00;
            d = -2.2543374567856151E+00;
            f = 2.6914441182441431E+00;
        case 'GLN'
            a = 1.9005567570863825E+01;
            b = -2.0319122385487057E-02;
            c = -1.2083581566255768E+01;
            d = -1.4429406093358430E+00;
            f = 9.7256610585859082E+00;
        case 'ASP'
            a = 2.0166013924521497E+01;
            b = -4.7853403267681283E-02;
            c = -6.9784835381725507E+00;
            d = -1.9994797588755993E+00;
            f = 3.2626175074094688E+00;
        case 'GLU'
            a = 1.9232515360604562E+01;
            b = 3.5086173204513208E-03;
            c = -1.2596769305364237E+01;
            d = -4.9237630472478966E-01;
            f = 9.3597483117941245E+00;
        case 'LYS'
            a = 1.0962754376635287E+01;
            b = -3.1372344486261888E-02;
            c = 7.3756682779136329E-01;
            d = -3.0693869455133744E+00;
            f = 5.0544680881861872E+00;
        case 'ARG'
            a = 2.3271735452541201E+01;
            b = 6.7314944953355738E-01;
            c = -3.6116270352466870E+01;
            d = 2.1362171853330082E+01;
            f = 1.6290629563851400E+01;
        case 'HIS'
            a = 2.1448835902382307E+01;
            b = -9.8889323535974627E-02;
            c = -7.0305612380447311E+00;
            d = -4.4298193773300865E+00;
            f = 6.2697213262776561E+00;
    end
    F(sph,:) = a + b*q + c*q.^2 + d*q.^3 + f*q.^4;
end

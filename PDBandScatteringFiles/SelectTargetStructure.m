
function Target = SelectTargetStructure(Source,Name)
%
%SelectTargetStructure
%
%This function loads the Crysol, Matlab or FoXS scattering data and returns
%it in the Target parameter struct.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Target = SelectTargetStructure(Source,Name)
%   INPUTS
%       Source: String with the source of the protein. MAT, CrysolOutput
%       and FoXS implemented.
%       Name: String with the file name to be loaded.
%   OUTPUTS
%       Target: Struct with the Target options.



disp('Loading Target Structure...');
if strcmpi(Source,'PDB')
    
elseif strcmpi(Source,'MAT')
    Data = load(Name);
    Target.Iexp = Data.Data(:,2);
    Target.q = Data.Data(:,1);
        disp('Target Structure Loaded Successfully');

elseif strcmpi(Source,'CrysolOutput')
    Data = dlmread(Name,' ',1,2); %Read the file
    Target.Iexp = Data(:,3); %Save the Scattering
    Target.q = Data(:,1); %Save the q values
    if Target.q(1) == 0
        Target.Iexp = Target.Iexp(2:end);
        Target.q = Target.q(2:end);
    end
    disp('Target Structure Loaded Successfully');
elseif strcmpi(Source,'FoXS')
    Data = dlmread(Name,' ',2,0); %Read the file
    Target.Iexp = Data(:,5); %Save the Scattering
    Target.q = Data(:,1); %Save the q values
    if Target.q(1) == 0
        Target.Iexp = Target.Iexp(2:end);
        Target.q = Target.q(2:end);
    end
    disp('Target Structure Loaded Successfully');
else
    disp('Unable to load Target Structure');
    return
end

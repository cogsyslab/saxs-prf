function Start = SelectStartStructure(Source,Name)
%
%SelectStartStructure
%
%This function parses the PDB file of the chosen protein and returns the
%required data in the Start parameter struct.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Start = SelectStartStructure(Source,Name)
%   INPUTS
%       Source: String with the source of the protein. Only PDB
%               implemented.
%       Name: String with the file name to be loaded, without '.pdb'
%             extension
%   OUTPUTS
%       Start: Struct with the Start options.

disp('Loading Start Structure...');
if strcmpi(Source,'PDB')
    Start.Name = Name; % Add Name Field
    PDB = pdbread([Name,'.pdb']); % Load PDB file
    Start.Nres = PDB.Sequence(1).NumOfResidues; % Add Nres Field
    Start.Structure = LoadCoarseModelFromPDB(PDB); %Add Coarse Model Structure
    Start.Nres = length(Start.Structure); % Correcy Nres Field
    [Start.Hlx, Start.BtS] = LoadSecondaryStructFromPDB(PDB); %Add the Secondary Structure Information
    Start.AminoLabels =strsplit(PDB.Sequence(1).ResidueNames)'; %Add the label of each aminoacid
    disp('Start Structure Loaded Successfully');
    
elseif strcmpi(Source,'File')

else
disp('Unable to load Start Structure');
end
end

function CoarseModel = LoadCoarseModelFromPDB(PDB)

    Letter = PDB.Model.Terminal(1).chainID;
    AtomLabels = extractfield(PDB.Model.Atom,'AtomName'); %Get the Atom types
    AtomChain =  extractfield(PDB.Model.Atom,'chainID'); %Get the Chain ID
    CAindices = find((strcmp(AtomLabels,'CA').*strcmp(AtomChain,Letter))>0); %Get the indices for the C-alphas in chain A
    CoarseModel = zeros(length(CAindices),3); %Create the CoarseModel matrix
    CoarseModel(:,1) = extractfield(PDB.Model.Atom(CAindices),'X')'; %Fill the X coordinates
    CoarseModel(:,2) = extractfield(PDB.Model.Atom(CAindices),'Y')'; %Fill the Y coordinates
    CoarseModel(:,3) = extractfield(PDB.Model.Atom(CAindices),'Z')'; %Fill the Z coordinates
    
end

function [Hlx, BtS] = LoadSecondaryStructFromPDB(PDB)
    Letter =PDB.Model.Terminal(1).chainID;
    NHlx = sum(strcmp(extractfield(PDB.Helix,'initChainID'),Letter)); %Find the number of Helixes
    NBtS = sum(strcmp(extractfield(PDB.Sheet,'initChainID'),Letter));   %Find the number of sheets
    Hlx = zeros(2,NHlx);    %Create the Hlx matrix
    BtS = zeros(2,NBtS);    %Create the Bts matrix
    Hlx(1,:) = extractfield(PDB.Helix(1:NHlx),'initSeqNum'); %Fill the start of the Helixes
    Hlx(2,:) = extractfield(PDB.Helix(1:NHlx),'endSeqNum'); %Fill the end of the Helixes
    BtS(1,:) = extractfield(PDB.Sheet(1:NBtS),'initSeqNum');    %Fill the start of the Sheets
    BtS(2,:) = extractfield(PDB.Sheet(1:NBtS),'endSeqNum');   %Fill the end of the Sheets
end
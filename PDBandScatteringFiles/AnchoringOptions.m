
function Anchoring = AnchoringOptions(varargin)
%
%AnchoringOptions
%
%This function loads the Spring section of the Param struct with the
%options relative to the ESM module.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Anchoring = AnchoringOptions(varargin)
%   INPUTS
%       Start: Struct with the Start options.
%       OPTIONAL: 4 arguments more with the 4 indices of the anchors.
%   OUTPUTS
%       Anchoring: Struct with the Anchoring options

Start = varargin{1}; %Start Struct is the first variable

if nargin < 5 %If user didn't supplied enough anchor points
    disp('Attempting Default Anchoring'); %We pick 4 consecutive points in the largest Alpha Helix
    HlxLenght = Start.Hlx(2,:)-Start.Hlx(1,:); %Determine the Length of the Helixes
    Longest = find(HlxLenght == max(HlxLenght),1,'first'); %Determine the longest
    FirstAnchor = randi([Start.Hlx(1,Longest) Start.Hlx(2,Longest)-3],1,1); %Select one at random
    Anchors = FirstAnchor:FirstAnchor + 3; %Select the next 3 also
else
     disp('Attempting User-Supplied Anchoring');
    Anchors = [varargin{2:5}]; %Pick the entries 2 to 5
end

AncorIndx = zeros(12,1); %Create the index vector
ZPMat = zeros(3*Start.Nres,3*Start.Nres-12); %Create the Zero Padding Matrix

for i = 1:4
    AncorIndx((i-1)*3+1:3*i) = 3*(Anchors(i)-1)*ones(3,1) + [1:3]'; %Fill the vector
end

TempVec = ones(3*Start.Nres,1); %We create an all-ones vector
TempVec(AncorIndx) = 0; %We put zeros on the entries of the anchored elements.
ZPMat(find(TempVec ==1),:) = eye(3*Start.Nres-12); %ZPMat has ones on the diagonal elements of the non-anchored atoms
xl = reshape(Start.Structure',3*Start.Nres,1); %We vectorize the structure
ZFVec = zeros(3*Start.Nres,1); %We create the Zero Filling Vector
ZFVec(AncorIndx) = xl(AncorIndx); %Its non-zero entries are the values of the anchored atoms
x = xl(find(TempVec ==1)); %Our truncated variable is the structure without the anchored atoms

Anchoring.ZPMat = ZPMat;
Anchoring.ZFVec = ZFVec;
Anchoring.xl = xl;
Anchoring.x = x;
disp('Anchoring Successfull');

    
    
    


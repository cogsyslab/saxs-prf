%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        Protein Refinement Framework - Roig-Solvas et al.            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Select Initial PDB file:
InitialStructure = '4AKE';

% Select Target Scattering:
TargetScattering = 'AKeco_1AKE_A00.int';

% Initialize the Framework:
Param = InitializeFramework(InitialStructure,TargetScattering);

% Run the Refinement Algorithm:
Param = ADMM(Param);

% Generate a PDB file with the Refined C-alpha coordinates
PDBfilename = 'testname.pdb';
SavePDBfile(Param,PDBfilename);
function [Iest] = DebyeFormula(X,F,D,Namino,q)
%
%DebyeFormula
%
%This function computes the SAXS profile of a set of amino acid
%coordinates and form factors using the Debye Formula.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[Iest] = DebyeFormula(X,F,D,Namino,q)
%   INPUTS
%       X: Matrix of amino acid coordinates for variable "x".
%       F: Matrix with the form factor of each amino acid.
%       D: The Euclidean Distance Matrix of the amino acid coordinates
%       Namino: Number of amino acids in the protein
%       q: Vector of SAXS q scattering angle values.
%   OUTPUTS
%       Iest: Estimated scattering intensity as a function of q.

N = length(X);
Q = length(q);
Iprot = zeros(Q,1);
Iwat = zeros(Q,1);
Icross = zeros(Q,1);
for sph1 = 1:Namino
    for sph2 = (sph1+1):Namino
        Iprot = Iprot + (F(sph1,:).*F(sph2,:))'.*sin(q*D(sph1,sph2))./(q*D(sph1,sph2));
    end
end
for sph1 = Namino+1:N
    for sph2 = (sph1+1):N
        Iwat = Iwat + (F(sph1,:).*F(sph2,:))'.*sin(q*D(sph1,sph2))./(q*D(sph1,sph2));
    end
end
for sph1 = 1:Namino
    for sph2 = Namino+1:N
        Icross = Icross + (F(sph1,:).*F(sph2,:))'.*sin(q*D(sph1,sph2))./(q*D(sph1,sph2));
    end
end
Iprot = Iprot*2;
Iwat = Iwat*2;
Icross = Icross*2;

for sph = 1:Namino
    Iprot = Iprot + (F(sph,:).^2)';
end
for sph = Namino+1:N
    Iwat = Iwat + (F(sph,:).^2)';
end

Iprot = Iprot*4*pi;
Iwat = Iwat*4*pi;
Icross = Icross*4*pi;

Iest = Iprot + Iwat + Icross;

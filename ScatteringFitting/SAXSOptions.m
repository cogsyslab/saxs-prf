function Fitting = SAXSOptions(varargin)
%
%SAXSOptions
%
%This function loads the Fitting section of the Param struct with the
%options relative to the SAXS module.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Fitting = SAXSOptions(varargin)
%   INPUTS
%       Param: Struct with the Param parameters.
%       name: String with the field to be changed.
%       value: Value to be supplied to that field.
%   OUTPUTS
%       Fitting: Struct with the SAXS options.

Param = varargin{1};
FittingFields = {'F';'Fc'}; % Define the name of the editable fields to be written in the struct

Fitting.F  = AminoFormFactor(Param.Start.AminoLabels,Param.Target.q);
Fitting.Fc = 1;

%Rewrite those that the user especified
for field = 1:length(FittingFields)
    if sum(strcmpi(FittingFields{field},varargin)) > 0 %If there is a coincidence with the input and a field name
        ValueIndx = find(strcmpi(FittingFields{field},varargin) == 1,1,'first'); %Search which is the coincidence
        Fitting.(FittingFields{field}) = varargin{ValueIndx + 1}; %Edit that field with the value supplied by the user
    end
end


function [grad,Jacobian] = SAXSGeval(X,D,F,e,Namino,q)
%
%SAXSGeval
%
%This function evaluates the SAXS fitting function gradient and returns
%such gradient and the Jacobian.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[grad,Jacobian] = SAXSGeval(X,D,F,e,Namino,q)
%   INPUTS
%       X: Matrix of amino acid coordinates for variable "x".
%       D: The Euclidean Distance Matrix of the amino acid coordinates.
%       F: Matrix with the form factor of each amino acid.
%       e: Fitting error vector
%       Namino: Number of amino acids in the protein
%       q: Vector of SAXS q scattering angle values.
%   OUTPUTS
%       Jacobian: Jacobian of the SAXS fitting function at X.
%       grad: The gradient of the SAXS fitting function at X.
   



N = length(D);
Q = length(q);
Jacobian = zeros(3*Namino,Q);

%correct q dimensions
if size(q,2) > size(q,1)
    q = q';
end

%Build the Jacobian
for sph1 = 1:Namino
    for sph2 = sph1+1:Namino
        d = D(sph1,sph2);
        Cont = F(sph1,:).*F(sph2,:)/d;
        Trig = cos(q*d)./d - sin(q*d)./(q.*d^2);
        Coordif = (X(sph1,:)-X(sph2,:));
        Block = Coordif'*(Trig'.*Cont);
        Jacobian((sph1-1)*3+1:(sph1-1)*3+3,:) = Jacobian((sph1-1)*3+1:(sph1-1)*3+3,:) + Block;
        Jacobian((sph2-1)*3+1:(sph2-1)*3+3,:) = Jacobian((sph2-1)*3+1:(sph2-1)*3+3,:) -Block;
    end
    for sph2 = Namino+1:N
        d = D(sph1,sph2);
        Cont = F(sph1,:).*F(sph2,:)/d;
        Trig = cos(q*d)./d - sin(q*d)./(q.*d^2);
        Coordif = (X(sph1,:)-X(sph2,:));
        Block = Coordif'*(Trig'.*Cont);
        Jacobian((sph1-1)*3+1:(sph1-1)*3+3,:) = Jacobian((sph1-1)*3+1:(sph1-1)*3+3,:) + Block;
    end
end

Jacobian = Jacobian*4*pi;

%Compute the gradient
grad = 2*Jacobian*e;
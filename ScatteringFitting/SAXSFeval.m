function [sqne,Iest,e,D] = SAXSFeval(X,F,Iexp,Namino,q)
%
%SAXSFeval
%
%This function evaluates the SAXS fitting function and returns
%estimated scattering at point X, the error and its squared norm and the
%Euclidean Pairwise Distance matrix.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[sqne,Iest,e,D] = SAXSFeval(X,F,Iexp,Namino,q)
%   INPUTS
%       X: Matrix of amino acid coordinates for variable "x".
%       F: Matrix with the form factor of each amino acid.
%       Iexp: The experimental SAXS intensity to be fitted.
%       Namino: Number of amino acids in the protein
%       q: Vector of SAXS q scattering angle values.
%   OUTPUTS
%       sqne: Squared norm of the fitting error.
%       Iest: Estimated scattering intensity as a function of q.
%       e: Fitting error vector
%       D: The Euclidean Distance Matrix of the amino acid coordinates




% Compute the pairwise distances between amino acids
D = sqrt(PairwiseDistance(X'));

% Correct Iexp dimensions to make it a column vector
if size(Iexp,2) > size(Iexp,1)
    Iexp = Iexp';
end

% Compute the Scattering profile of the amino acid coordinates
[Iest] = DebyeFormula(X,F,D,Namino,q);

% Normalize wrt the zero angle of the scattering
%Iexp = Iexp/Iexp(1)*Iest(1);

% Compute the residual and its squared 2-norm
e = Iest-Iexp;
sqne = e'*e;
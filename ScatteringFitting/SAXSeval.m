function [f,grad,Hessian] = SAXSeval(x,z,y,rho,Param)
%
%SAXSeval
%
%This function evaluates the SAXS fitting function, gradient and
%Hessian as a function of "x" and "Param" and builds the function, gradient
%and Hessian of the Lagrangian wrt the variable "x".
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[f,grad,Hessian] = SAXSeval(x,z,y,rho,Param)
%   INPUTS
%       x: The vectorized split variable x, corresponding to the SAXS function.
%       z: The vectorized split variable z, corresponding to the ESM function.
%       y: The Lagrangian dual variable y.
%       rho: The Augmented Lagrangian variable rho.
%       Param: Struct with the Framework Parameters.
%   OUTPUTS
%       f: The Lagrangian value w.r.t "x" at the point "x".
%       grad: The Lagrangian gradient w.r.t "x" at the point "x".
%       Hessian: The Lagrangian Hessian w.r.t "x" at the point "x".

% Extract the variables from the Param struct
N = Param.Start.Nres;
ZPMat = Param.Anchoring.ZPMat;
ZFVec = Param.Anchoring.ZFVec;
F = Param.Fitting.F;
q = Param.Target.q;
Iqexp = Param.Target.Iexp;

% Reshape the x vector into a matrix
X = reshape(ZPMat*x+ZFVec,3,N)';

% Add the water samples around the protein
[S,Fs] = HydrationShellv2(X,Param.Water.NSubd,Param.Water.NSam,Param.Water.WSThk,Param.Water.SCThk,F,q,Param);

% Evaluate the SAXS function
[sqne,~,e,D] = SAXSFeval(S,Fs,Iqexp,N,q);

% Evaluate the SAXS gradient
[gradFit,Jacobian] = SAXSGeval(S,D,Fs,e,N,q);

% Evaluate the SAXS Hessian
Hessian = SAXSHeval(S,D,Fs,e,N,q);

% Compute the objective function, gradient and Hessian of the Laplacian
f = Param.Fitting.Fc*sqne + y'*x + (rho/2)*(x-z)'*(x-z);
grad = Param.Fitting.Fc*ZPMat'*gradFit + y + rho*(x-z);
Hessian = Param.Fitting.Fc*ZPMat'*(2*Hessian + 2*(Jacobian*Jacobian'))*ZPMat + rho*eye(3*N-12,3*N-12);









function Hessian = SAXSHeval(X,D,F,e,Namino,q)
%
%SAXSHeval
%
%This function evaluates the SAXS fitting function Hessian and returns
% the Hessian matrix.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Hessian = SAXSHeval(X,D,F,e,Namino,q)
%   INPUTS
%       X: Matrix of amino acid coordinates for variable "x".
%       D: The Euclidean Distance Matrix of the amino acid coordinates.
%       F: Matrix with the form factor of each amino acid.
%       e: Fitting error vector
%       Namino: Number of amino acids in the protein
%       q: Vector of SAXS q scattering angle values.
%   OUTPUTS
%       Hessian: Hessian of the SAXS fitting function at point X.
   

N = length(D);
Q = length(q);
Hessian = zeros(3*Namino,3*Namino);

% Build the Hessian
for sph1 = 1:Namino
    for sph2 = sph1+1:Namino
        %Build the Block off-diagonals
        d = D(sph1,sph2);
        TrigoOff = ((sin(d*q).*(3./(q*d^3)-q/d) - 3*cos(q*d)/(d^2))./(d^2)).*F(sph1,:)'.*F(sph2,:)';
        Block = -(X(sph1,:)-X(sph2,:))'*(X(sph1,:)-X(sph2,:))*(e'*TrigoOff);
        %Build the Block diagonal:
        TrigoDiag = (sin(d*q)./(q*d) - cos(q*d))/(d^2).*F(sph1,:)'.*F(sph2,:)';
        Block = Block + diag((e'*TrigoDiag)*ones(3,1));
        Hessian((sph1-1)*3+1:(sph1-1)*3+3,(sph2-1)*3+1:(sph2-1)*3+3) = Block;
        Hessian((sph1-1)*3+1:(sph1-1)*3+3,(sph1-1)*3+1:(sph1-1)*3+3) = Hessian((sph1-1)*3+1:(sph1-1)*3+3,(sph1-1)*3+1:(sph1-1)*3+3) - 0.5*Block;
        Hessian((sph2-1)*3+1:(sph2-1)*3+3,(sph2-1)*3+1:(sph2-1)*3+3) = Hessian((sph2-1)*3+1:(sph2-1)*3+3,(sph2-1)*3+1:(sph2-1)*3+3) - 0.5*Block;
    end
    for sph2 = Namino+1:N
        %Build the Block off-diagonals
        d = D(sph1,sph2);
        TrigoOff = ((sin(d*q).*(3./(q*d^3)-q/d) - 3*cos(q*d)/(d^2))./(d^2)).*F(sph1,:)'.*F(sph2,:)';
        Block = -(X(sph1,:)-X(sph2,:))'*(X(sph1,:)-X(sph2,:))*(e'*TrigoOff);
        %Build the Block diagonal:
        TrigoDiag = (sin(d*q)./(q*d) - cos(q*d))/(d^2).*F(sph1,:)'.*F(sph2,:)';
        Block = Block + diag((e'*TrigoDiag)*ones(3,1));
        Hessian((sph1-1)*3+1:(sph1-1)*3+3,(sph1-1)*3+1:(sph1-1)*3+3) = Hessian((sph1-1)*3+1:(sph1-1)*3+3,(sph1-1)*3+1:(sph1-1)*3+3) - 0.5*Block;
    end
end
Hessian = Hessian + Hessian';
Hessian = Hessian*4*pi;


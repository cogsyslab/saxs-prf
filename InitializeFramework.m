function Param = InitializeFramework(InitialStructure,TargetScattering)
%
%InitializeFramework
%
%This function initializes the parameters of each section of the Param struct. 
%The default values can be found in each of the called functions (WaterOptions, AnchoringOptions, ...) and those
%values can be changed when calling the function, by using a two argument syntax like: WaterOptions('name',value), where name is the name of the
%parameter and value the value that the user wants to set up for that parameter.
%
%Author: Biel Roig-Solvas
%Last Edit: January 21st, 2016.
% 
%Param = InitializeFramework(InitialStructure,TargetScattering)
%   INPUTS
%       InitialStructure: String with the name of the PDB file to be used.
%       TargetScattering: String with the name of the crysol ".int" file to
%       be used.
%   OUTPUTS
%       Param: Struct with the Framework Parameters




% Add all subfolders
addpath('./ElasticSubdomainModel/')
addpath('./OptimizationFramework/')
addpath('./PDBandScatteringFiles/')
addpath('./ScatteringFitting/')
addpath('./WaterModel/')

% Set Start and Target Structure
Param.Start = SelectStartStructure('PDB',InitialStructure);
Param.Target = SelectTargetStructure('CrysolOutput',TargetScattering);

% Set Water Model Options
Param.Water = WaterOptions;

% Set Anchoring Options
Param.Anchoring = AnchoringOptions(Param.Start);

% Set Scattering Fitting Options
Param.Fitting = SAXSOptions(Param);

% Set Elastic Subdmain Model Options
Param.Spring = ESMOptions(Param.Start);

% Set ADMM Options
Param.ADMM = ADMMOptions(Param);

% Set Relative Weight Alpha (more alpha, more scattering fitting
% importance)
Param = SetFunctionWeighting(0.5,Param); 


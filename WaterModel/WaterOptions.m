function Water = WaterOptions(varargin)

WaterFields = {'NSubd';'NSam';'WSThk';'SCThk';'PartitionMethod'}; % Define the name of the fields to be written in the struct
Water.NSubd = 5; %Define the default parameters
Water.NSam = 1000; %Define the default parameters
Water.WSThk = 2; %Define the default parameters
Water.SCThk = 4; %Define the default parameters
Water.PartitionMethod = 'Kmeans'; %Define the default parameters

%Rewrite those that the user especified
for field = 1:length(WaterFields)
    if sum(strcmpi(WaterFields{field},varargin)) > 0 %If there is a coincidence with the input and a field name
        ValueIndx = find(strcmpi(WaterFields{field},varargin) == 1,1,'first'); %Search which is the coincidence
        Water.(WaterFields{field}) = varargin{ValueIndx + 1}; %Edit that field with the value supplied by the user
    end
end
        
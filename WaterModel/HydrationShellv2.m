function [S,Fs] = HydrationShellv2(X,Nsub,Nsam,Wt,SCt,F,q,Param)

if strcmpi(Param.Water.PartitionMethod,'Kmeans')
    idx = kmeans(X,Nsub);
    %disp('kmeans')
else
    warning off
    GMModel = fitgmdist(X,Nsub,'Options',statset('Display','off'),'RegularizationValue',0.1);
    warning on
    idx = cluster(GMModel,X);
    %disp('gmm')
end
for i = 1:Nsub
    Subdomains{i} = find(idx==i);
end
[Wat,TotVol] = ComputeWaterShell5(X,Subdomains,Wt, Nsam,SCt);
sollabels = cell(length(Wat),1);
for kk = 1:length(Wat)
    sollabels{kk} = 'SOL';
end
Fsol = AminoFormFactor(sollabels,q);
Nelec = TotVol*0.03;
Fsol = Fsol/(sum(Fsol(:,1)))*Nelec;
S = [X;Wat];
Fs = [F;Fsol];

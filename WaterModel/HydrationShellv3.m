function [S,Fs] = HydrationShellv3(X,Nsub,Nsam,Wt,SCt,F,q,Param)

% Cluster the X amino acids in Nsub clusters
idx = kmeans(X,Nsub);

% Save each group in a cell
for i = 1:Nsub
    Subdomains{i} = find(idx==i);
end

% Initialize variables
Vertices = cell(Nsub,1);
Centroids = cell(Nsub,1);
Inflation1 = zeros(Nsub,1);
Inflation2 = zeros(Nsub,1);
Faces = cell(Nsub,1);
ShellVolume = zeros(Nsub,1);
Points = zeros(Nsam*Nsub,3);
% For each Subdomain
for ii = 1:Nsub
    Set = X(Subdomains{ii},:); %Take amino acids from ii'th cluster
    [K,V] = convhull(Set); %Compute its hull
    Faces{ii} = K;
    Vertices{ii} = Set(unique(reshape(K,size(K,1)*size(K,2),1)),:); %Take the vertices of the hull
    Centroids{ii} = mean(Vertices{ii});
    MeanVertexLength = mean(sqrt(PairwiseDistance(Vertices{ii}',Centroids{ii}')));
    Inflation1(ii) = (MeanVertexLength + SCt)/MeanVertexLength;
    Inflation2(ii) = (MeanVertexLength + SCt + Wt)/MeanVertexLength;
    ShellVolume(ii) = (Inflation2(ii)^3-Inflation1(ii)^3)*V;
    %Compute Points, Somehow.
end

for ii = 1:Nsub
    [Insiders, Outsiders] = InsideHull(K{ii},Inflation1(ii)*Vertices{ii},Points);
    %Remove Insiders.
end
for ii = 1:Nsub
    [Insiders, Outsiders] = InsideHull(K{ii},Inflation2(ii)*Vertices{ii},Points);
    %Remove Insiders
end


sollabels = cell(length(Wat),1);
for kk = 1:length(Wat)
    sollabels{kk} = 'SOL';
end
Fsol = AminoFormFactor(sollabels,q);
Nelec = TotVol*0.03;
Fsol = Fsol/(sum(Fsol(:,1)))*Nelec;
S = [X;Wat];
Fs = [F;Fsol];

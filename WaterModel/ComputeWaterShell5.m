%% Compute Water Shell
function [Wat,TotVol] = ComputeWaterShell5(Coor,Subdomains, WshellThck, Samplingnum,SCshellThck)
if nargin < 5
    SCshellThck = 0;
end
%% Break Coor into Subdomains.
%Get the number of subdomains
NSub = length(Subdomains);
%CH is the cell of convex hulls
CH = cell(NSub,1);
%FaceSet is the cell of faces
FaceSet = cell(NSub,1);
%This is the cell of vertices
vertices = cell(NSub,1);
verticeslist = cell(NSub,1);
expcoor = cell(NSub,1);
%The vector of volumes of convex hulls
CHVol = zeros(NSub,1);
ShellVol = zeros(NSub,1);
ExposedVol = zeros(NSub,1);
%The vector of Centroids;
VertCentroids = zeros(NSub,3);
%The cell of water points
WaterPoints = cell(NSub,1);
RejectedWaterPoints = cell(NSub,1);
%Vector of ExpansionValues
Expansion = zeros(NSub,1);
Npoints = Samplingnum;
%% For each Subdomain
for ii = 1:NSub
    %% Compute its convex hull and save results
    Set = Coor(Subdomains{ii},:);
    [K,V] = convhull(Set);
    CH{ii} = K;
    CHVol(ii) =  V;
    %% Compute its vertices and its Centroid.
    tempvertices = Set(unique(reshape(K,size(K,1)*size(K,2),1)),:);
    vertices{ii} = tempvertices;
    verticeslist{ii} = unique(reshape(K,size(K,1)*size(K,2),1));
    VertCentroids(ii,:) = mean(tempvertices);
    [WaterPoints{ii},ShellVol(ii)] = CohFS2(Set,K,V,Npoints,SCshellThck,SCshellThck+WshellThck,'abs','abs');
   
end
% ShellPoints = zeros(NSub,1);
% for ii = 1:NSub
%     ShellPoints(ii) = round(Npoints*ShellVol(ii)/sum(ShellVol));
%     WaterPoints{ii} = CohFS(Set,K,ShellPoints(ii),1,Expansion(ii));
% end
%% Reject points 
for ii = 1:NSub
    %Compute the normal vectors and the normal offsets for all faces of CH
    %ii
    K = CH{ii};
    NormalVec = zeros(length(K),3);
    FacePoint = zeros(length(K),3);
    for kk = 1:length(NormalVec);
        NormalVec(kk,:) = cross(Coor(Subdomains{ii}(K(kk,1)),:)-Coor(Subdomains{ii}(K(kk,2)),:),Coor(Subdomains{ii}(K(kk,1)),:)-Coor(Subdomains{ii}(K(kk,3)),:));
        NormalVec(kk,:) = NormalVec(kk,:)/norm(NormalVec(kk,:));
        FacePoint(kk,:) = Coor(Subdomains{ii}(K(kk,1)),:);
    end
    %% We project the centroid of CH(i) onto those vectors to get all of them pointing outside (positive out).
    Proj = NormalVec*VertCentroids(ii,:)' - diag(NormalVec*FacePoint');
    ProjSign = -(Proj)./abs(Proj);
    NormalVec = diag(ProjSign)*NormalVec;
    for jj = 1:NSub
        if jj~=ii
        points = WaterPoints{jj};
        %% Project the rejected points on those vectors
        rejprojection = NormalVec*points' - repmat(diag(NormalVec*FacePoint'),1,size(points,1));
        %result = sum((rejprojection>0));
        result = sum((rejprojection>SCshellThck));
        keptpoints = find(result>0);    
        WaterPoints{jj} = points(keptpoints,:);
        end
    end        
end

Wat = [];
for ii = 1:NSub
    %ExposedVol(ii) = size(WaterPoints{ii},1)/ShellPoints(ii)*ShellVol(ii);
    ExposedVol(ii) = size(WaterPoints{ii},1)/Npoints*ShellVol(ii);
end
for ii = 1:NSub
    if ExposedVol(ii)/sum(ExposedVol)*Samplingnum<size(WaterPoints{ii},1)
        points = WaterPoints{ii};
        indices= randsample(size(points,1),ceil(ExposedVol(ii)/sum(ExposedVol)*Samplingnum));
        WaterPoints{ii} = points(indices,:);
    else
    end
    Wat = [Wat;WaterPoints{ii}];
end
Wat = Wat(randsample(size(Wat,1),Samplingnum),:);
TotVol = sum(ExposedVol);

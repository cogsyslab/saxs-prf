%% COnvex Hull Frustum Sampling
function [Samples,SampledVolume] = CohFS2(V,Triangulation,Vol,ns,rmin,rmax,rminscale,rmaxscale)
if nargin <6
    rminscale = 'rel';
    rmaxscale = 'rel';
end
Nfaces = size(Triangulation,1);
Vcenter = mean(V);
V = V - repmat(Vcenter,size(V,1),1);
%% Compute the Area of each Face and the Volume of each Frustum.
FaceArea = zeros(Nfaces,1);
pyramheight = zeros(Nfaces,1);
FrustumVolume = zeros(Nfaces,1);
for ii =1:Nfaces
    %Extract the sides of the pyramid created by the face and the Hull center
    a = V(Triangulation(ii,1),:);
    b = V(Triangulation(ii,2),:);
    c = V(Triangulation(ii,3),:);
    %Extract the length of its sides
    s1 = sqrt((a-b)*(a-b)');
    s2 = sqrt((c-b)*(c-b)');
    s3 = sqrt((a-c)*(a-c)');
    %Use Heron's Formula to get the area of the face
    p = 0.5*(s1+s2+s3);
    FaceArea(ii) = sqrt(p*(p-s1)*(p-s2)*(p-s3));
    %Compute the height of the pyramid by projecting one point on the face
    %onto the vector normal to the face
    normvec = cross(a-b,a-c);
    normvec = normvec/norm(normvec);
    pyramheight(ii) = abs(normvec*a');
end
weightedmeanheight = pyramheight'*(FaceArea.*pyramheight)./sum((FaceArea.*pyramheight));
if strcmpi(rminscale,'rel')
    rmin = rmin;
elseif strcmpi(rminscale,'abs')
    if rminscale+weightedmeanheight<0
        disp('Absolute rmin too small')
    return
    else
        rmin = (rmin+weightedmeanheight)/weightedmeanheight;
    end
else
disp('rminscale not recognized')
    return
end
if strcmpi(rmaxscale,'rel')
    rmax = rmax;
elseif strcmpi(rmaxscale,'abs')
    if rmaxscale+weightedmeanheight<0
        disp('Absolute rmax too small')
    return
    else
        rmax = (rmax+weightedmeanheight)/weightedmeanheight;
    end
else
disp('rmaxscale not recognized')
    return
end
    
    
    
pyramvolume = (FaceArea.*pyramheight)/3;
FrustumVolume = (rmax^3-rmin^3)*pyramvolume;
TotalFrustumVolume = sum(FrustumVolume);
FrustumVolumeSamplingLine = cumsum(FrustumVolume);

%% Sample the ns points in the Volume.
SamplingFrustum = rand(ns,1)*TotalFrustumVolume;
Samples = zeros(ns,3);
for ii=1:ns
    %Decide in which Frustum we sample the point
    SampledFrustum = find(FrustumVolumeSamplingLine>SamplingFrustum(ii),1,'first');
    %Decide at which range we sample the point
    range =   (rmin^3 + rand(1,1)*(rmax^3-rmin^3))^(1/3);
    %Extract the sides of the pyramid created by the face and the Hull center
    a = V(Triangulation(SampledFrustum,1),:);
    b = V(Triangulation(SampledFrustum,2),:);
    c = V(Triangulation(SampledFrustum,3),:);
    %Sample the point from the Face of the Polytope
    r1 = rand(1,1); r2 = rand(1,1);
    Samples(ii,:) = ((1 - sqrt(r1))*a + (sqrt(r1)*(1 - r2))*b+ (sqrt(r1)*r2)*c)*range;
end

Samples = Samples + repmat(Vcenter,ns,1);
SampledVolume = (rmax^3-rmin^3)*Vol;
    

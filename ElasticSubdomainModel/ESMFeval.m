function [spe,dc] = ESMFeval(D,h,K,Indx)
%
%ESMFeval
%
%This function evaluates the Elastic Subdomain Model function and returns
%the spring system's potential energy and the length of the springs.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[spe,dc] = ESMFeval(D,h,K,Indx)
%   INPUTS
%       D: The Euclidean Distance Matrix of the amino acid coordinates
%       h: The equilibrium distances of the system's springs.
%       K: The vector of elastic constants of the springs.
%       Indx: Element pair correspondence for each spring.
%   OUTPUTS
%       spe: Spring potential energy.
%       dc: Length of the springs at the present iteration.

Nc = length(h);
dc = zeros(Nc,1);

for ii= 1:Nc
    dc(ii) = D(Indx(ii,1),Indx(ii,2));
end
spe = (dc-h)'.*K*(dc-h);
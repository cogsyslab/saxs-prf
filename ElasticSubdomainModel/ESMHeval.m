function [Hessiand,HessianObj] = ESMHeval(Z,h,K,Indx,dc,Jacobian)
%
%ESMHeval
%
%This function evaluates the Hessian of the Elastic Subdomain Model function 
%and returns the gradient and Jacobian.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[Hessiand,HessianObj] = ESMHeval(Z,h,K,Indx,dc,Jacobian)
%   INPUTS
%       Z: Matrix of amino acid coordinates for variable "z".
%       h: The equilibrium distances of the system's springs.
%       K: The vector of elastic constants of the springs.
%       Indx: Element pair correspondence for each spring.
%       dc: Length of the springs at the present iteration.
%       Jacobian: Jacobian of the ESM function at Z.
%   OUTPUTS
%       Hessiand: Hessian of the ESM function at Z.
%       HessianObj: Sum of the Hessian and the K-weighted Gramian of the
%                   Jacobian.

Nc = length(h);
N = size(Z,1);
Hessiand = zeros(3*N,3*N);

for i = 1:Nc

    k = Indx(i,1);
    l = Indx(i,2);
    k1 = (k-1)*3+1;
    k3 = (k-1)*3+3;
    l1 = (l-1)*3+1;
    l3 = (l-1)*3+3;
    dc3 = dc(i)^3;
    %Build k-k submatrix and l-k submatrix
    KKSubH = zeros(3,3);
    KKSubH(1,2) = -(Z(k,1)-Z(l,1))*(Z(k,2)-Z(l,2))/dc3;
    KKSubH(1,3) = -(Z(k,1)-Z(l,1))*(Z(k,3)-Z(l,3))/dc3;
    KKSubH(2,3) = -(Z(k,2)-Z(l,2))*(Z(k,3)-Z(l,3))/dc3;
    KKSubH = KKSubH + KKSubH';
    KKSubH(1,1) = -(Z(k,1)-Z(l,1))^2/dc3+1/dc(i);
    KKSubH(2,2) = -(Z(k,2)-Z(l,2))^2/dc3+1/dc(i);
    KKSubH(3,3) = -(Z(k,3)-Z(l,3))^2/dc3+1/dc(i);
    LLSubH = KKSubH;
    %Build k-l submatrix
    KLSubH = -KKSubH;
    Hessiand(k1:k3,l1:l3) = Hessiand(k1:k3,l1:l3) + (dc(i)-h(i))*K(i)*KLSubH;
    Hessiand(l1:l3,k1:k3) = Hessiand(l1:l3,k1:k3) + (dc(i)-h(i))*K(i)*KLSubH;
    Hessiand(k1:k3,k1:k3) = Hessiand(k1:k3,k1:k3) + (dc(i)-h(i))*K(i)*KKSubH;
    Hessiand(l1:l3,l1:l3) = Hessiand(l1:l3,l1:l3) + (dc(i)-h(i))*K(i)*LLSubH;
end

HessianObj = Hessiand + (Jacobian.*repmat(K,size(Jacobian,1),1))*Jacobian';
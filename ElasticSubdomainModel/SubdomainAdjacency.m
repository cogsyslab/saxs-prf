function Adj = SubdomainAdjacency(Start,k)
%
%SubdomainAdjacency
%
%This function return the Adjacency matrix of the graph obtained by a
%K-means clustering of the amino acid coordinates plus an added edge
%between adjacent amino acids in the protein chain.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Adj = SubdomainAdjacency(Start,k)
%   INPUTS
%       Start: Struct with the Start options.
%       k: Number of subdomains.
%   OUTPUTS
%       Adj: Binary adjacency matrix.

Prot = Start.Structure;
N = Start.Nres;
[IDX] = kmeans(Prot,k);
labels = zeros(N,N);
for i = 1:k
labels = labels + i*abs(IDX==i)*abs(IDX==i)';
end
A = labels>0;
A = A + diag(ones(N-1,1),1);
A = A + A';
A = A>0;
A = A - diag(diag(A));
Adj = A;

function [Jacobian,grad] = ESMGeval(Z,h,K,Indx,dc)
%
%ESMGeval
%
%This function evaluates the gradient of the Elastic Subdomain Model function 
%and returns the gradient and Jacobian.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[Jacobian,grad] = ESMGeval(Z,h,K,Indx,dc)
%   INPUTS
%       Z: Matrix of amino acid coordinates for variable "z".
%       h: The equilibrium distances of the system's springs.
%       K: The vector of elastic constants of the springs.
%       Indx: Element pair correspondence for each spring.
%       dc: Length of the springs at the present iteration.
%   OUTPUTS
%       Jacobian: Jacobian of the ESM function at Z.
%       grad: The gradient of the ESM function at Z.
      

Nc = length(h);
N = size(Z,1);
Jacobian = zeros(3*N,Nc);
for ii=1:Nc
    k = Indx(ii,1);
    l = Indx(ii,2);
    val = (Z(k,:)-Z(l,:))'/dc(ii);
    Jacobian((k-1)*3+1:(k-1)*3+3,ii) = val;
    Jacobian((l-1)*3+1:(l-1)*3+3,ii) = -val;
end
grad = Jacobian*(K'.*(dc-h));
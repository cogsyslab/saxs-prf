function [f,grad,Hessian] = ESMeval(z,x,y,rho,Param)
%
%ESMeval
%
%This function evaluates the Elastic Subdomain Model function, gradient and
%Hessian as a function of "z" and "Param" and builds the function, gradient
%and Hessian of the Lagrangian wrt the variable "z".
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%[f,grad,Hessian] = ESMeval(z,x,y,rho,Param)
%   INPUTS
%       z: The vectorized split variable z, corresponding to the ESM function.
%       x: The vectorized split variable x, corresponding to the SAXS function.
%       y: The Lagrangian dual variable y.
%       rho: The Augmented Lagrangian variable rho.
%       Param: Struct with the Framework Parameters.
%   OUTPUTS
%       f: The Lagrangian value w.r.t "z" at the point "z".
%       grad: The Lagrangian gradient w.r.t "z" at the point "z".
%       Hessian: The Lagrangian Hessian w.r.t "z" at the point "z".

h = Param.Spring.h;
K = Param.Spring.K;
Indx = Param.Spring.Indx;
ZPMat = Param.Anchoring.ZPMat;
ZFVec = Param.Anchoring.ZFVec;

N = length(ZPMat*z+ZFVec)/3;
Z = reshape(ZPMat*z+ZFVec,3,N)';
D = sqrt(PairwiseDistance(Z'));

[spe,dc] = ESMFeval(D,h,K,Indx);
[Jacobian,grad] = ESMGeval(Z,h,K,Indx,dc);
[Hessiand,HessianObj] = ESMHeval(Z,h,K,Indx,dc,Jacobian);

f = Param.Spring.Kc*spe - y'*z + (rho/2)*(x-z)'*(x-z);
grad = Param.Spring.Kc*ZPMat'*2*grad - y + rho*(z-x);
Hessian = Param.Spring.Kc*ZPMat'*(2*HessianObj)*ZPMat + rho*eye(3*N-12,3*N-12);

function Spring = ESMOptions(varargin)
%
%ESMOptions
%
%This function loads the Spring section of the Param struct with the
%options relative to the ESM module.
%
%Author: Biel Roig-Solvas
%Last Edit: January 23rd, 2016.
% 
%Spring = ESMOptions(varargin)
%   INPUTS
%       Start: Struct with the Start options.
%       name: String with the field to be changed.
%       value: Value to be supplied to that field.
%   OUTPUTS
%       Spring: Struct with the ESM options

Start = varargin{1}; %Start Struct is the first variable
SpringFields = {'HlxBond';'HlxOffset';'BtSBond';'BtSOffset';'CalphaBond';'CalphaOffset';'BtSheetBond';'BtSheetThr';'Kc';'SubdomainA';'SubdomainK';'A'}; % Define the name of the editable fields to be written in the struct
Spring.HlxBond = 10; %Define the default parameters
Spring.HlxOffset = 3; %Define the default parameters
Spring.BtSBond = 10; %Define the default parameters
Spring.BtSOffset = 2; %Define the default parameters
Spring.CalphaBond = 10; %Define the default parameters
Spring.CalphaOffset = 0; %Define the default parameters
Spring.BtSheetBond = 10; %Define the default parameters
Spring.BtSheetThr = 15; %Define the default parameters
Spring.SubdomainA = true; %Define the default parameters
Spring.SubdomainK = 3; %Define the default parameters
Spring.Kc = 100; %Define the default parameters
Spring.A = [];


%Rewrite those that the user especified
for field = 1:length(SpringFields)
    if sum(strcmpi(SpringFields{field},varargin)) > 0 %If there is a coincidence with the input and a field name
        ValueIndx = find(strcmpi(SpringFields{field},varargin) == 1,1,'first'); %Search which is the coincidence
        Spring.(SpringFields{field}) = varargin{ValueIndx + 1}; %Edit that field with the value supplied by the user
    end
end


if max(size(Spring.A))==0 %If user didn't provide the A matrix
    if Spring.SubdomainA %Create the matrix using the subdomain approach
        Spring.A = SubdomainAdjacency(Start,Spring.SubdomainK);
    else %Or using the secondary structure elements    
        Spring.A = ProteinAdjacency(Start.Structure, Start.Hlx, Start.BtS, Spring);
    end
end
%Generate the rest of the parameters
[Spring.K,Spring.Indx,Spring.h] = GenerateParameters(Start.Structure,Spring.A);

end

function [K,Indx,h] = GenerateParameters(Structure,A)
%Find the number of edges bigger than the 0. Discard Half, as A is
%symmetric.
Nc = 0.5*length(find(A > 0));
K = zeros(1,Nc);
Indx = zeros(Nc,2);
h = zeros(Nc,1);
D = sqrt(PairwiseDistance(Structure'));
%Fill the variables
count = 1;
for i = 1:size(Structure,1)
    for j= i+1:size(Structure,1)
        if A(i,j)>0
            K(count) = A(i,j);
            Indx(count,:) = [i,j];
            h(count) = D(i,j);
            count = count +1;
        end
    end
end

end

function A = ProteinAdjacency(Structure, Hlx, BtSht, Spring)
A = zeros(length(Structure),length(Structure));
%Fill the Hlx Entries
offset = Spring.HlxOffset;
for ii = 1: size(Hlx,2)
   for jj = Hlx(1,ii):(Hlx(2,ii)-offset)
       A(jj,jj+1:jj+offset) = Spring.HlxBond;
   end
   for kk = 1:offset-1
   A(jj+kk,jj+kk+1:jj+offset) = Spring.HlxBond;
   end
   
end

% Fill the BtS entries

offset = Spring.BtSOffset;
for ii = 1: size(BtSht,2)
   for jj = BtSht(1,ii):(BtSht(2,ii)-offset)
       A(jj,jj+1:jj+offset) = Spring.BtSBond;
   end
   for kk=1:offset-1
   A(jj+kk,jj+kk+1:jj+offset) = Spring.BtSBond;
   end
end

%Fill the Calpha entries

A = A + diag(Spring.CalphaBond*ones(length(Structure)-1,1),1);
for offset = 1:Spring.CalphaOffset
    A = A -diag(A,1) + diag(Spring.CalphaBond*ones(length(Structure)-(1+offset),1),(1+offset));
end

%Make the matrix symmetric
A = A + A';

%Add the BetaSheet Constrains
BetaIndex = zeros(length(A),1);
for i = 1:length(BtSht)
BetaIndex(BtSht(1,i):BtSht(2,i)) = i;
end
DBeta = BetaSheetPWDtest(Structure,BetaIndex);
DBeta = ((DBeta>0).*(DBeta<Spring.BtSheetThr))*Spring.BtSheetBond;
A = A + DBeta;

end